FROM alpine:3.12
LABEL description="Ansible on Alpine 3.12 and Python 3"

RUN apk -U add gcc make python3 python3-dev openssl-dev && \
    apk -U add py3-cffi py3-bcrypt py3-cryptography py3-pynacl py3-pip && \
    apk -U add git g++ libffi-dev libxml2-dev libxslt-dev && \
    pip3 install --upgrade pip setuptools && \
    pip3 install ansible && \
    pip3 install pyvmomi && \
    pip3 install requests && \
    pip3 install --upgrade git+https://github.com/vmware/vsphere-automation-sdk-python.git
    
CMD ["ansible", "--version"]